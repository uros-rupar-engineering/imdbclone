//
//  BannerCell.swift
//  imdbClone
//
//  Created by uros.rupar on 6/17/21.
//

import UIKit

class BannerCell: UICollectionViewCell {
    
    @IBOutlet weak var buttonForAddingToList: UIButton!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleMovie: UILabel!
    @IBOutlet weak var secondaryImage: UIImageView!
    @IBOutlet weak var typeMovie: UILabel!
}
