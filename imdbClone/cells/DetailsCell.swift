//
//  DetailsCell.swift
//  imdbClone
//
//  Created by uros.rupar on 6/21/21.
//

import UIKit

class DetailsCell: UICollectionViewCell {
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var detailsCell: UIView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var buttonForAddingToWatchList: UIButton!
    @IBOutlet weak var rate: UILabel!
    
}
