//
//  StreamingCell.swift
//  imdbClone
//
//  Created by uros.rupar on 6/22/21.
//

import UIKit

class StreamingCell: UICollectionViewCell {
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var buttonWatchListed: UIButton!
    
    
}
