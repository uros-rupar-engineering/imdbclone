//
//  SecondCollectionCell.swift
//  imdbClone
//
//  Created by uros.rupar on 6/18/21.
//

import UIKit

class SecondCollectionCell: UICollectionViewCell {
    @IBOutlet weak var mainImgae: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
}
