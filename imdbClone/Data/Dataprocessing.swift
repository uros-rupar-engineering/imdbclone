//
//  Dataprocessing.swift
//  imdbClone
//
//  Created by uros.rupar on 6/17/21.
//

import Foundation


class DataProcessing{
    
    // api
   static let videoSliderApi = "http://www.omdbapi.com/?apikey=e709e37&s=taxi"
    static let featuredTodayApi = "http://www.omdbapi.com/?apikey=e709e37&s=harry"
    static let topPicsForYouApi = "http://www.omdbapi.com/?apikey=e709e37&s=mil"
    static let whatToWatchApi = "http://www.omdbapi.com/?apikey=e709e37&s=car"
    static let funFavouritesApi = "http://www.omdbapi.com/?apikey=e709e37&s=ter"
    static let streamingApi = "http://www.omdbapi.com/?apikey=e709e37&s=ter"
    

    
    //list
    static var movieArr: [MovieShort]? = []
    static var featuredTodayArr:[MovieShort]? = []
    static var topPicsForYouArr :[MovieShort]? = []
    static var whatToWatchArr :[MovieShort]? = []
    static var funFavouritesArr :[MovieShort]? = []
    static var streamingArr :[MovieShort]? = []

  
    //method for fetching datas
    static func fetchMovie(url:String, completion: @escaping (SearchMovies?) -> Void){
        
        guard let apiURL = URL(string: url) else {return}
        
        let task =  URLSession.shared.dataTask(with: apiURL){
            (data,response,error) in
            if let data =  data , case let movies = try?
            
                
                JSONDecoder().decode(SearchMovies.self, from: data){
                
                completion(movies)
            }
        }
        
        task.resume()
        
    }
    
 
    
    
    
}
