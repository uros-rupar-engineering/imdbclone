//
//  MovieModel.swift
//  imdbClone
//
//  Created by uros.rupar on 6/17/21.
//4a73ebbe

import Foundation

struct MovieShort: Codable{

    var title: String

    var year: String

    var imdbID: String
    
    var type : String

    var poster: String
    
    enum CodingKeys: String, CodingKey{
        case title = "Title"
        case year = "Year"
        case imdbID
        case type = "Type"
        case poster = "Poster"

      
    }
}



struct SearchMovies: Codable{

   var search: [MovieShort]

    var totalResults: String

    var response: String
    
    enum CodingKeys: String, CodingKey{
        case search = "Search"
        case totalResults
        case response = "Response"
     
    }

}



var searchMovie: SearchMovies?


struct Movie: Codable{
    var title:String
    var year:String
}
