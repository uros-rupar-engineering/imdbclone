//
//  AppColors.swift
//  imdbClone
//
//  Created by uros.rupar on 6/16/21.
//

import Foundation
import UIKit

class AppColors{
    
   static let tabBarColor = UIColor(red: 0.20, green: 0.20, blue: 0.22, alpha: 1.00)
    
    static let primary = UIColor(red: 0.11, green: 0.11, blue: 0.11, alpha: 1.00)
    
    static let buttomSignIn = UIColor(red: 0.62, green: 0.51, blue: 0.11, alpha: 1.00)

}
