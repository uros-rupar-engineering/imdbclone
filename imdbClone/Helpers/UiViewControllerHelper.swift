//
//  UiViewControllerHelper.swift
//  imdbClone
//
//  Created by uros.rupar on 6/21/21.
//

import Foundation
import UIKit

class UIViewControllerHelper {
    
    static func makeRoundButton(button: UIButton,stepen: CGFloat,borderColor:UIColor,borderWidth:CGFloat,title : String){
        button.layer.cornerRadius = stepen
        button.layer.borderColor = borderColor.cgColor
        button.layer.borderWidth = borderWidth
        button.setTitle(title, for: .normal)
        
    }
}
