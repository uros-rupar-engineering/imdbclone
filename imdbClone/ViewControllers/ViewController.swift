//
//  ViewController.swift
//  imdbClone
//
//  Created by uros.rupar on 6/16/21.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    //timer
    var autoScrollTimer = Timer()

    
    //collection view
    @IBOutlet weak var videoVievHolder: UICollectionView!
    @IBOutlet weak var FeaturedTodaySlider: UICollectionView!
    @IBOutlet weak var TopPicsForYou: UICollectionView!
    @IBOutlet weak var WhatToWatch: UICollectionView!
    @IBOutlet weak var FunFavourites: UICollectionView!
    @IBOutlet weak var Streaming: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareDataForBanner()

    }
   
     func prepareDataForBanner() {
        
        // 1. Video slider data
        DataProcessing.fetchMovie(url: DataProcessing.videoSliderApi) { (result) in
            guard let result = result else { return }
                DataProcessing.movieArr = result.search
            
            DispatchQueue.main.async {
                self.videoVievHolder.reloadData()
             
            }
           
        }
        
        // 2. featured Today data
        DataProcessing.fetchMovie(url: DataProcessing.featuredTodayApi) { (result) in
            guard let result = result else { return }
            DataProcessing.featuredTodayArr = result.search

            DispatchQueue.main.async {
                self.FeaturedTodaySlider.reloadData()
             
            }
     
        }
        
        // 3. TopPicsForYou data
        DataProcessing.fetchMovie(url: DataProcessing.whatToWatchApi) { (result) in
            guard let result = result else { return }
            DataProcessing.whatToWatchArr = result.search
            
            DispatchQueue.main.async {
                self.WhatToWatch.reloadData()
             
            }
     
        }
        
        // 4. WhatToWatch data
        DataProcessing.fetchMovie(url: DataProcessing.topPicsForYouApi) { (result) in
            guard let result = result else { return }
            DataProcessing.topPicsForYouArr = result.search
           
            DispatchQueue.main.async {
                self.TopPicsForYou.reloadData()
             
            }
     
        }
        
        // 5. WhatToWatch data
        DataProcessing.fetchMovie(url: DataProcessing.funFavouritesApi) { (result) in
            guard let result = result else { return }
            DataProcessing.funFavouritesArr = result.search
          
            DispatchQueue.main.async {
                self.FunFavourites.reloadData()
             
            }
     
        }
        
        // 6. Streaming data
        DataProcessing.fetchMovie(url: DataProcessing.streamingApi) { (result) in
            guard let result = result else { return }
            DataProcessing.streamingArr = result.search
          
            DispatchQueue.main.async {
                self.Streaming.reloadData()
             
            }
     
        }

        
        
    }
    
    
    
    
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        switch collectionView {
        
        case videoVievHolder:
            return DataProcessing.movieArr?.count ?? 0
            
        case FeaturedTodaySlider:
            
        return DataProcessing.featuredTodayArr?.count ?? 0
            
        case TopPicsForYou:
            
        return  DataProcessing.topPicsForYouArr?.count ?? 0
            
        case WhatToWatch:
            
        return  DataProcessing.whatToWatchArr?.count ?? 0
            
        case FunFavourites:
            
            return  DataProcessing.funFavouritesArr?.count ?? 0
            
        case Streaming:
            
            return  DataProcessing.streamingArr?.count ?? 0
             
        default:
            return  0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        switch collectionView {
        case videoVievHolder:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BannerCell
            if let urlString = DataProcessing.movieArr![indexPath.row].poster as? String,let url = URL(string: urlString){
                URLSession.shared.dataTask(with: url){(data,response,error) in
                    if let data = data{

                        DispatchQueue.main.async {
                            cell.mainImage.image = UIImage(data: data)
                            cell.secondaryImage.image = UIImage(data: data)
   
                        }
                    }
                }.resume()
            }
            
            
            
            cell.titleMovie.text = DataProcessing.movieArr![indexPath.row].title
            
            cell.typeMovie.text = DataProcessing.movieArr![indexPath.row].type
            
            cell.buttonForAddingToList.layer.cornerRadius = 0
            
            var indexRow = indexPath.row
            
            let numberOfRecords:Int = DataProcessing.movieArr!.count - 1
            
            if indexRow < numberOfRecords{
                indexRow = (indexRow + 1)
                print(indexRow)
            }else{
                indexRow = 0
            }
            
            autoScrollTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self,selector: #selector(ViewController.startTimer(theTimer:)), userInfo: indexRow, repeats: true)
            
            return cell
            
            
            
        case FeaturedTodaySlider:
            
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cella", for: indexPath) as! SecondCollectionCell
            
            if let urlString = DataProcessing.featuredTodayArr![indexPath.row].poster as? String,let url = URL(string: urlString){
                URLSession.shared.dataTask(with: url){(data,response,error) in
                    if let data = data{

                        DispatchQueue.main.async {
                            cell1.mainImgae.image = UIImage(data: data)
                        }
                    }
                }.resume()
            }
            
            cell1.descriptionLabel.text = "The best book-to-Screen Adaptions in the Works"
            
            return cell1
            
        case TopPicsForYou :
            
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "cellDetails", for: indexPath) as! DetailsCell
            
            if let urlString = DataProcessing.topPicsForYouArr![indexPath.row].poster as? String,let url = URL(string: urlString){
                URLSession.shared.dataTask(with: url){(data,response,error) in
                    if let data = data{

                        DispatchQueue.main.async {
                            cell2.mainImage.image = UIImage(data: data)
                        }
                    }
                }.resume()
                
               
            }
            
            cell2.rate.text = String(round(Double.random(in: 1..<10) * 10) / 10.0)
            cell2.title.text = DataProcessing.topPicsForYouArr![indexPath.row].title
            
            cell2.info.text = "\(DataProcessing.topPicsForYouArr![indexPath.row].year)  description "
            
            UIViewControllerHelper.makeRoundButton(button: cell2.buttonForAddingToWatchList, stepen: 5, borderColor: .lightGray, borderWidth: 2, title: " +WatchList")
           
            

            
           
            
            return cell2
            
        case WhatToWatch:
            
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "WhatToWatchCell", for: indexPath) as! WhatToWatchCell
            
            if let urlString = DataProcessing.whatToWatchArr![indexPath.row].poster as? String,let url = URL(string: urlString){
                URLSession.shared.dataTask(with: url){(data,response,error) in
                    if let data = data{

                        DispatchQueue.main.async {
                            cell3.imageMain.image = UIImage(data: data)
                        }
                    }
                }.resume()
                
               
            }
            
            cell3.rate.text = String(round(Double.random(in: 1..<10) * 10) / 10.0)
            cell3.title.text = DataProcessing.whatToWatchArr![indexPath.row].title
            
            cell3.info.text = "\(DataProcessing.whatToWatchArr![indexPath.row].year)  description "
            
            UIViewControllerHelper.makeRoundButton(button: cell3.buttonWatchListed, stepen: 5, borderColor: .lightGray, borderWidth: 2, title: " WatchListed")
            
            return cell3
            
        
        case FunFavourites:
            
            let cell4 = collectionView.dequeueReusableCell(withReuseIdentifier: "funFavouritesCell", for: indexPath) as! FanFavouritesCell
            
            if let urlString = DataProcessing.funFavouritesArr![indexPath.row].poster as? String,let url = URL(string: urlString){
                URLSession.shared.dataTask(with: url){(data,response,error) in
                    if let data = data{

                        DispatchQueue.main.async {
                            cell4.imageMain.image = UIImage(data: data)
                        }
                    }
                }.resume()
                
               
            }
            
            cell4.rate.text = String(round(Double.random(in: 1..<10) * 10) / 10.0)
            cell4.title.text = DataProcessing.funFavouritesArr![indexPath.row].title
            
            cell4.info.text = "\(DataProcessing.funFavouritesArr![indexPath.row].year)  description "
            
            UIViewControllerHelper.makeRoundButton(button: cell4.buttonWatchListed, stepen: 5, borderColor: .lightGray, borderWidth: 2, title: " WatchListed")
            
            return cell4
            
        case Streaming:
            
            let cell5 = collectionView.dequeueReusableCell(withReuseIdentifier: "streamingCell", for: indexPath) as! StreamingCell
            
            if let urlString = DataProcessing.streamingArr![indexPath.row].poster as? String,let url = URL(string: urlString){
                URLSession.shared.dataTask(with: url){(data,response,error) in
                    if let data = data{

                        DispatchQueue.main.async {
                            cell5.mainImage.image = UIImage(data: data)
                        }
                    }
                }.resume()
                
               
            }
            
            cell5.rate.text = String(round(Double.random(in: 1..<10) * 10) / 10.0)
            cell5.title.text = DataProcessing.streamingArr![indexPath.row].title
            
            cell5.info.text = "\(DataProcessing.streamingArr![indexPath.row].year)  description "
            
            UIViewControllerHelper.makeRoundButton(button: cell5.buttonWatchListed, stepen: 5, borderColor: .lightGray, borderWidth: 2, title: " WatchListed")
            
            return cell5

            
        default:
            return UICollectionViewCell()
        }
        
        //whatch list
        
        
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        switch collectionView {
        case videoVievHolder:
            let width = videoVievHolder.frame.width
            let height = videoVievHolder.frame.height

            return CGSize(width: width, height: height)
        case FeaturedTodaySlider:
            let width = FeaturedTodaySlider.frame.width
            let height = FeaturedTodaySlider.frame.height * 0.7

            return CGSize(width: width, height: height)
            
        case TopPicsForYou:
            
            let width = TopPicsForYou.frame.width * 0.38
            let height = TopPicsForYou.frame.height

            return CGSize(width: width, height: height)
            
        case WhatToWatch:
            let width = TopPicsForYou.frame.width * 0.38
            let height = TopPicsForYou.frame.height

            return CGSize(width: width, height: height)
            
        case FunFavourites:
            let width = TopPicsForYou.frame.width * 0.38
            let height = TopPicsForYou.frame.height

            return CGSize(width: width, height: height)
            
        case Streaming:
            let width = TopPicsForYou.frame.width * 0.38
            let height = TopPicsForYou.frame.height

            return CGSize(width: width, height: height)
        
        
        default:
            return CGSize(width: 0, height: 0)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
       
    }
    
    
    @objc func startTimer(theTimer:Timer){
        UIView.animate(withDuration: 2.0, delay: 0, options: .curveEaseOut, animations: {
            
            self.videoVievHolder.scrollToItem(at: IndexPath(row: theTimer.userInfo! as! Int, section: 0), at: .centeredHorizontally, animated: true)
            
        }, completion: nil)
    }

    
  

}


